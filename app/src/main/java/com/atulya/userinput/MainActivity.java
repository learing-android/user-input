package com.atulya.userinput;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void handleInput(View view){

        EditText editView = findViewById(R.id.editText);

        String input =  editView.getText().toString();

        Log.d("atul", input);

        TextView textView = findViewById(R.id.textView);

        textView.setText("Welcome" + input + "!");

        Toast.makeText(this, "Yay!!", Toast.LENGTH_SHORT).show();

    }
}